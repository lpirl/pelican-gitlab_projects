SITENAME = 'gitlab_projects example'
TIMEZONE = 'Europe/Rome'
AUTHOR = 'Alex'

PATH = 'content'
THEME_TEMPLATES_OVERRIDES = ['templates']

STATIC_PATHS = []

FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

PLUGINS = ['gitlab_projects']
GITLAB_USER = 'lpirl'
