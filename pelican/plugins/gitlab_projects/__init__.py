"""
this module is responsible for inserting the plug-in into Pelican
"""

from pelican import signals

from .gitlab_projects import initialize, add_to_context

def register():
  """ wires up Pelican signals and this plug-in functionality """
  signals.initialized.connect(initialize)
  signals.page_generator_context.connect(add_to_context)
  signals.article_generator_context.connect(add_to_context)
