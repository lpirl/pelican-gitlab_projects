"""
this module implements the plug-in functionality
"""

from functools import cache
from logging import getLogger

from gitlab import Gitlab


SETTINGS_DEFAULTS = {
  'GITLAB_URL': 'https://gitlab.com',
  'GITLAB_ORDER_BY': 'updated_at',
  'GITLAB_CONTRIBUTED_PROJECTS': False,
}

LOGGER = getLogger(__name__)


def unique_stable(iterable):
  """
  yield items in :param:`iterable` while preserving order and skipping
  duplicates
  """
  seen = set()
  for item in iterable:
    if item in seen:
      continue
    seen.add(item)
    yield item


@cache
def get_context(url, username, order_by, contributed):
  """ returns the data to add to a rendering context """

  gitlab = Gitlab(url)
  users = gitlab.users.list(username=username)
  if not users:
    LOGGER.warning('GitLab user "%s" not found', username)
    return []
  assert len(users) == 1
  user = users[0]

  list_kwargs = {
    'order_by': order_by,
    "per_page": 100,
    "iterator": True,
  }

  LOGGER.info('querying projects from GitLab')
  if contributed:
    events = user.events.list(type='Project', **list_kwargs)
    project_ids = unique_stable(e.project_id for e in events)
    projects = [gitlab.projects.get(project_id)
                for project_id in project_ids]
  else:
    projects = user.projects.list(**list_kwargs)
  LOGGER.info('queried details from GitLab')

  return projects


def add_to_context(generator, metadata=None):
  """ adds data acquired by this plug-in to rendering contexts """
  _ = metadata # silence warning about this specific unused argument
  settings = generator.settings
  generator.context['gitlab_projects'] = get_context(
    settings['GITLAB_URL'],
    settings['GITLAB_USER'],
    settings['GITLAB_ORDER_BY'],
    settings['GITLAB_CONTRIBUTED_PROJECTS'],
  )


def initialize(pelican):
  """ does some sanity checking of settings and sets defaults """

  if 'GITLAB_USER' not in pelican.settings:
    LOGGER.warning('GITLAB_USER not set')

  for setting, value in SETTINGS_DEFAULTS.items():
    pelican.settings.setdefault(setting, value)
