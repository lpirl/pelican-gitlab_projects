=======================
Pelican GitLab projects
=======================

Embed a list of your GitLab projects in your `Pelican
<https://getpelican.com>`__ articles or pages.


Installation
============

Installation is easiest via pip and git:

.. code-block:: sh

  $ pip install git+https://gitlab.com/lpirl/pelican-gitlab_projects.git


Configuration
=============

Enable the plugin in your ``pelicanconf.py``:

.. code-block:: python

  PLUGINS = [
    …
    'gitlab_projects',
    …
  ]

Add a variable containing your GitLab username in the settings:

.. code-block:: python

  GITLAB_USER = 'foobar'

Add optional settings to your liking:

.. code-block:: python

  GITLAB_URL = "https://gitlab.example.com" # (default is gitlab.com)

  # see ``order_by`` at https://docs.gitlab.com/ee/api/projects.html
  GITLAB_ORDER_BY = "name" # (default is update time)

  # list projects GITLAB_USER has contributed to instead or just own projects
  GITLAB_CONTRIBUTED_PROJECTS = True # (default is False)


Usage
=====

Templates can iterate over the variable ``gitlab_projects`` to retrieve
data, e.g.:

.. code-block:: html

  {% if gitlab_projects %}
    {% for project in gitlab_projects %}
      <section id="{{ project.path_with_namespace }}">
        <h3><a href="{{ project.web_url }}"
            >{{ project.path_with_namespace }}</a></h3>
          {% if project.description %}
          <p>
            {{ project.description }}
          </p>
        {% endif %}
      </section>
    {% endfor %}
  {% endif %}

For a minimal working example, see the `example directory
<example>`__.

`A list of attributes available can be found in the GitLab docs
<https://docs.gitlab.com/ee/api/projects.html>`__.
