from setuptools import setup
from setuptools import find_namespace_packages

setup(
  name='gitlab_projects',
  version='0.1.0',
  url='https://gitlab.com/lpirl/pelican-gitlab_projects',
  download_url='https://gitlab.com/lpirl/pelican-gitlab_projects/-/archive/main/pelican-gitlab_projects-main.tar.bz2',
  author="Lukas Pirl",
  author_email="pelican-gitlab_projects@lukas-pirl.de",
  description="Embed a list of your public GitLab projects in your Pelican articles or pages",
  long_description=open("README.rst").read(),
  install_requires=[
    'pelican',
    'python-gitlab',
  ],
  packages=find_namespace_packages(include=['pelican.*']),
  classifiers=[
    'Development Status :: 4 - Beta',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.1',
    'Programming Language :: Python :: 3.2',
    'Programming Language :: Python :: 3.3',
    'Programming Language :: Python :: Implementation :: CPython',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
    'Topic :: Internet :: WWW/HTTP',
    'Topic :: Software Development :: Libraries :: Python Modules',
    'Topic :: Text Processing',
  ],
  zip_safe=True,
)
