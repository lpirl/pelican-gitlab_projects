MAKEFLAGS += -r

help:
	cat Makefile

test:
	$(MAKE) -C example _example-own-projects
	grep -q lpirl/ example/output/index.html
	$(MAKE) -C example _example-contributed-projects
	grep -q hpi-potsdam/osm example/output/index.html

qa: pylint

pylint:
	pylint --rcfile=.pylintrc pelican/plugins/gitlab_projects

clean:
	@find \( -name __pycache__ -or -name '*.pyc' \) -print0 \
	| xargs -0 rm -vfr
	@rm -vfr build
